'use strict';
const HtmlWebpackTemplate = require("html-webpack-plugin");

class WebpackOptionBuilder
{
    constructor(options) {
        this.options = options;
    }
    addTemplates(templates) {
        templates.forEach((template) => {
            this.options.plugins.push(new HtmlWebpackTemplate(template));
        });
    }
}
module.exports = WebpackOptionBuilder;