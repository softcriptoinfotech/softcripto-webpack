'use strict';
/**
 * Webpack module Rule declaration.
 */
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    /**
     * Return rules for SASS.
     * HMR enabled if mode == development
     * 
     * @function
     * @param {string} mode production | development
     */
    sass: function (mode) {
        if (!mode) {
            throw new Error("arg mode is expected as 'development' or 'production'");
        }
        return {
            test: /\.(sa|sc|c)ss$/,
            use: [
                {
                    loader: MiniCssExtractPlugin.loader,
                    options: {
                        hmr: mode === 'development' ? true : false
                    }
                },
                {
                    loader: 'css-loader',
                    options: {
                        sourceMap: true
                    }
                },
                {
                    loader: 'sass-loader',
                    options: {
                        sourceMap: true
                    }
                }
            ]
        }
    },
    js: {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [{
            loader: "babel-loader",
            options: {
                presets: ["env", "stage-0"],
                cacheDirectory: true,
                retainLines: true
            }
        }]
    },
    image: {
        test: /\.(png|svg|jpg|jpeg|gif)$/,
        use: [{
            loader: 'file-loader',
            options: {
                outputPath: 'img/',
                name: '[name].[ext]'
            }
        }]
    },
    html: {
        test: /\.html$/,
        use: {
            loader: "html-loader",
            options: {
                interpolate: true
            }
        }
    }
};