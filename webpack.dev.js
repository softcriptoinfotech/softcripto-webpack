/**
 * Webpack configuration for Development mode
 */
"use strict";

const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const rules = require("./rules");

/**
 * Mode
 */
let mode = 'development';
/**
 * 
 * @param {string} outputPath absolute path of container
 * @param {Number=} port - Dev server port number
 * @param {Boolean=} writeToDisk - bundles to disk instead of memory if pass true
 */
function webpackConfig(outputPath, port = 8080, writeToDisk = false) {
    return {
        target: 'web',
        mode,
        devtool: 'cheap-module-eval-source-map',
        devServer: {
            contentBase: outputPath,
            hot: true,
            host: "0.0.0.0",
            port: port,
            writeToDisk
        },
        entry: {},
        output: {
            filename: "[name].bundle.js",
            chunkFilename: '[name].bundle.js',
            path: outputPath
        },
        module: {
            rules: [rules.sass(mode), rules.js, rules.image, rules.html]
        },
        plugins: [
            new CleanWebpackPlugin(),
            new webpack.HotModuleReplacementPlugin(),
            new MiniCssExtractPlugin({
                filename: '[name].css',
                chunkFilename: '[id].css'
            })
        ]
    };
}
module.exports = webpackConfig;