/**
 * Test webpack
 */

'use strict';
const path = require('path');
const WebpackDevConfig = require("./webpack.dev");
const webpack = WebpackDevConfig(path.resolve(__dirname, "client/build"), 8080, false);

module.exports = webpack;
