# @softcripto/webpack

Reusable snippets for **webpack 4.x**

## Install

```bash
npm i @softcripto/webpack --save
```

## Usage

Runs on port **8080** and writes build to **memory**. http://localhost:8080
```javascript
const {WebpackDevConfig} = require("@softcripto/webpack");
const webpack = WebpackDevConfig(path.resolve(__dirname, "path/to/build"));
```

### Run in different port

```javascript
const {WebpackDevConfig} = require("@softcripto/webpack");
const webpack = WebpackDevConfig(path.resolve(__dirname, "path/to/build"), 9000);
```

### Write build to disk

```javascript
const {WebpackDevConfig} = require("@softcripto/webpack");
const webpack = WebpackDevConfig(path.resolve(__dirname, "path/to/build"), 9000, true);
```
> Passing third argument as **true** webpack dev server writes build to disk instead of memory

## License

[MIT](http://vjpr.mit-license.org)