'use strict';
const WebpackDevConfig = require("./webpack.dev");
const WebpackModuleRules = require("./rules");
const WebpackOptimization = require("./optimization");
/**
 * @module Softcripto/webpack
 */
module.exports = {
    WebpackDevConfig,
    WebpackModuleRules,
    WebpackOptimization
};